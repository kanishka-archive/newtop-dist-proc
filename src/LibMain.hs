{-# LANGUAGE TemplateHaskell #-}
module LibMain where

import Control.Concurrent (threadDelay)
import Control.Distributed.Process
import Control.Distributed.Process.Closure
import Control.Distributed.Process.Node hiding (newLocalNode)
import Control.Distributed.Process.Backend.SimpleLocalnet
import Control.Monad (forM_, forM)
import Data.Map (Map)
import Data.Maybe (fromMaybe)
import Data.Semigroup ((<>))
import qualified Data.Map as M
import Data.Time.Clock.POSIX (getPOSIXTime)
import Lib
import Options.Applicative
import System.Environment (getArgs)
import System.IO (Handle, hGetContents)
import System.Process as PR

remotable ['memberStateProcWrap]

myRemoteTable :: RemoteTable
myRemoteTable = LibMain.__remoteTable initRemoteTable

----------------- CONFIGURE HERE ---------------------
killSchedule :: [(Int, [Int])]
killSchedule = [] -- [(1,[0])]

numSlaves :: Int
numSlaves = 2
-------------------------------------------------------

defaultHost :: String
defaultHost = "localhost"

masterPort :: Int
masterPort = 3000

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["slave", host, port] -> do
      backend <- initializeBackend host port myRemoteTable
      startSlave backend
    ["master", host, port, sendFor, waitFor, seed] -> do
      backend <- initializeBackend host port myRemoteTable
      startMaster backend (master backend sendFor waitFor seed killSchedule)
    _ -> do
      opts <- execParser opts
      let ports = (fmap show . take numSlaves) [(masterPort+1)..] 
      houts <- forM ports (createSlave defaultHost)
      threadDelaySecs 1
      callProcess
        "newtop-dist-proc-exe"
        [ "master"
        , defaultHost
        , show masterPort
        , (show . opSendFor) opts
        , (show . opWaitFor) opts
        , (show . fromMaybe 1 . opSeed) opts
        ]
      forM_
        houts
        (\h -> hGetContents h >>= putStr)
  where
    opts = info (optParser <**> helper)
             ( fullDesc
            <> progDesc "Launch master and slave processes for newtop implementation"
            <> header "newtop-dist-proc-exe - basic impl of newtop" )

data Options = Options
  { opSendFor :: Int
  , opWaitFor :: Int
  , opSeed :: Maybe Int
  }

optParser :: Parser Options
optParser =
  Options
    <$> option auto
          ( long "send-for"
         <> metavar "SENDFOR" )
    <*> option auto
          ( long "wait-for"
         <> metavar "WAITFOR" )
    <*> optional
          (option auto
            ( long "with-seed"
           <> metavar "WITHSEED" ))

createSlave :: String -> String -> IO Handle
createSlave host port = do
  (_, Just hout, _, _) <-
    PR.createProcess_
      "slave"
      (proc "newtop-dist-proc-exe" ["slave", host, port]) { std_out = CreatePipe }
  pure hout

master :: Backend -> String -> String -> String -> [(Int, [Int])] -> [NodeId] -> Process ()
master backend sendFor waitFor seed schedule slaves = do
  let sendForSecs = read sendFor :: Int
      waitForSecs = read waitFor :: Int
      totalSecs = sendForSecs + waitForSecs
      totalSecsSlave = sendForSecs + (maximum [(waitForSecs - 5), 0])
      seedVal = read seed :: Int
  t <- liftIO getPOSIXTime
  slaveProcs <-
     forM
       slaves
       ((flip spawn) ($(mkClosure 'memberStateProcWrap) (sendForSecs, totalSecsSlave, seedVal)))
  forM_ slaveProcs (\p -> send p slaveProcs)
  runKillSchedule slaveProcs schedule totalSecs
  terminateAllSlaves backend

runKillSchedule :: [ProcessId] -> [(Int,[Int])] -> Int -> Process ()
runKillSchedule slaves schedule secsRemain =
  case schedule of
    (waitSec, killList):schedule' -> do
      liftIO (threadDelaySecs waitSec)
      forM
        killList
        (\i -> exit (slaves !! i) ())
      runKillSchedule slaves schedule' (secsRemain - waitSec)
    [] ->
      liftIO (threadDelaySecs secsRemain)

threadDelaySecs :: Int -> IO ()
threadDelaySecs = threadDelay . (*1000000)
