{-# LANGUAGE RecordWildCards #-}
module Lib
    ( memberStateProcWrap
    ) where

import Control.Concurrent (threadDelay)
import Control.Distributed.Process
import Control.Distributed.Process.Serializable (Serializable)
import Control.Monad (when, forM_, forM)
import Control.Monad.State -- QUALIFY
import Data.List (partition, sortOn, zipWith, (\\), nub)
import qualified Data.Map as M
import Data.Map (Map)
import Data.Maybe (fromMaybe)
import Data.Ratio ((%))
import Data.Semigroup ((<>))
import Data.Set (Set)
import qualified Data.Set as S
import Data.Time.Clock.POSIX (getPOSIXTime, POSIXTime)
import Safe.Foldable (minimumMay, minimumDef, maximumDef)
import qualified System.Random as RA

trace :: Bool
trace = False

-- node state
data ProcConfig = ProcConfig {
    pcEndTime :: !POSIXTime
  , pcStopTime :: !POSIXTime
  , pcSuspectTimeoutSecs :: !Rational
  , pcSelf :: !ProcessId
  , pcRecvsPerSend :: !Int
  } deriving (Eq, Show)
  
data ProcState = ProcState { -- Member
    stClock :: !Integer
  , stReceiveVector :: !(Map ProcessId (Integer, POSIXTime))
  , stPendingDeliver :: ![(Double, Integer, ProcessId)]
  , stMemberView :: ![ProcessId] -- Set
  } deriving (Show)

data SuspectInfo = SuspectInfo {
    siSuspicion :: !(Map ProcessId Integer) -- my local suspicions
  , siGossip :: !(Map ProcessId (Map ProcessId Integer)) -- Map suspect (Map detector lastClock)
  , siEnabledViewUpdate :: !(Maybe (Integer, Set ProcessId))
  , siStabilityVector :: !(Map ProcessId Integer)
  , siUnstableDelivered :: ![(Double, Integer, ProcessId)]
  } deriving (Show)


---------------------- node top level
memberStateProcWrap :: (Int, Int, Int) -> Process ()
memberStateProcWrap (sendForSecs, totalSecs, seed) = do
  now <- liftIO getPOSIXTime
  let endTime = now + (fromIntegral sendForSecs)
      stopTime = now + (fromIntegral totalSecs)
  peers <- expect :: Process [ProcessId]
  dumpPeers peers
  self <- getSelfPid
  memberProc
    ProcConfig {
      pcEndTime = endTime
    , pcStopTime = stopTime
    , pcSuspectTimeoutSecs = (fromIntegral (length peers)) % 1
    , pcSelf = self
    , pcRecvsPerSend = 50 * (length peers)
    }
    (RA.mkStdGen seed)
    ProcState {
      stClock = 0
    , stReceiveVector = M.fromList (zip peers (repeat (0, now)))
    , stPendingDeliver = []
    , stMemberView = peers
    }
    SuspectInfo {
      siStabilityVector = M.empty
    , siUnstableDelivered = []
    , siSuspicion = M.empty
    , siGossip = M.empty
    , siEnabledViewUpdate = Nothing
    }
    (0, 0.0)

dumpPeers :: [ProcessId] -> Process ()
dumpPeers ps = do
  self <- getSelfPid
  if (not (null ps)) && head ps == self then say' "peers" ps else say' "peers" (length ps)

data Phase = Run | ReceiveOnly | Stop

determinePhase :: ProcConfig -> POSIXTime -> Phase
determinePhase cfg t
  | t < pcEndTime cfg = Run
  | t < pcStopTime cfg = ReceiveOnly
  | otherwise = Stop


whenExpiredDoReset :: Int -> Int -> Process a -> a -> Process (a, Int)
whenExpiredDoReset limit cnt act dflt =
  if cnt >= limit
    then do
      r <- act
      pure (r, 0)
    else pure (dflt, cnt + 1)

memberProc :: ProcConfig -> RA.StdGen -> ProcState -> SuspectInfo -> (Integer, Double) -> Process ()
memberProc cfg@ProcConfig{..} ranGen state suspectInfo bsum =
  go ranGen state suspectInfo bsum 0
  where
    go ranGen s si bsum tmr = do
      now <- liftIO getPOSIXTime
      case determinePhase cfg now of
        Stop -> 
          dumpProcState s si >> (liftIO . putStrLn . show) bsum
        ReceiveOnly -> do
          (si', s') <- receiveReady (si, s) (matchRecvSuspectProtocolMsg si s now)
          (mres, s'') <-
            receiveReady (Nothing, (s')) [ matchIf (notSuspectMsg si') (recvBroadcast' now (s', si')) ]
          let (delivered, (s''', si'')) = deliverRecordDisable si' (mres, s'')
          si''' <- checkRefuteGossip' si'' s'''
          go ranGen s''' si''' (updateBroadcastSum bsum delivered) tmr
        Run -> do
          -- when trace (say' "run -- " now)
          si' <- failureSuspector' s now si
          (si'', s') <- receiveReady (si', s) (matchRecvSuspectProtocolMsg si' s now)
          ((ranGen', s''), tmr')
            <- whenExpiredDoReset pcRecvsPerSend tmr (sendBroadcast' (ranGen, s')) (ranGen, s')
          (mres, s''') <-
            receiveReady (Nothing, (s'')) [ matchIf (notSuspectMsg si'') (recvBroadcast' now (s'', si'')) ]
          let (delivered, (s'''', si''')) = deliverRecordDisable si'' (mres, s''') 
          si'''' <- checkRefuteGossip' si''' s''''
          go ranGen' s'''' si'''' (updateBroadcastSum bsum delivered) tmr'
    matchRecvSuspectProtocolMsg suspectInfo state now =
      [ matchIf (notSuspect suspectInfo)
          (\msg ->
             let r = runState (recvSuspect pcSelf (stMemberView state) msg) suspectInfo
             in announceUpdateDetected pcSelf state r)
      , matchIf (notSuspect' suspectInfo)
          (\msg ->
             let r = runState (recvRefute suspectInfo now msg) state
             in forwardUpdateRefuted pcSelf suspectInfo r)
      , matchIf (notSuspect suspectInfo)
          (\msg ->
             let r = runState (recvConfirm pcSelf (stMemberView state) msg) suspectInfo
             in announceUpdateDetected pcSelf state r)
      ]
    failureSuspector' state now suspectInfo = do
      let notAlreadySuspected' =
            M.filterWithKey
             (\k _ -> not (k `elem` (M.keys . siSuspicion) suspectInfo))
             (stReceiveVector state)
          suspects = failureSuspector pcSuspectTimeoutSecs now notAlreadySuspected'
      when (trace && not (M.null suspects)) (say' "suspecting" suspects)
      mapM_ (mcast pcSelf (stMemberView state)) (M.toList suspects)
      pure suspectInfo { siSuspicion = siSuspicion suspectInfo <> suspects }
    sendBroadcast' (ranGen, state) = do
      execStateT  (sendBroadcast (mcastMsg pcSelf (stMemberView state))) (ranGen, state)
    recvBroadcast' now (state, suspectInfo) msg = do
      let r@(mr,s1) = runState (recvBroadcast now (siEnabledViewUpdate suspectInfo) msg) state
      when (trace && (stPendingDeliver state /= stPendingDeliver s1))
        (say' "recvd" (stPendingDeliver s1 \\ stPendingDeliver state, M.elems (stReceiveVector s1)))
      -- let d@(ds,(s,si)) = deliverRecordDisable suspectInfo r
      -- when (trace && (not (null ds))) (say' "delivered" (stPendingDeliver state \\ stPendingDeliver s))
      pure r
    checkRefuteGossip' suspectInfo state = do
      execStateT (checkRefuteGossip (\v -> mcastRefute pcSelf state suspectInfo v True) state) suspectInfo

updateBroadcastSum :: (Integer, Double) -> [Double] -> (Integer, Double)
updateBroadcastSum (n, vs) vals =
  let n' = fromIntegral n
      valsByIndex = (zipWith (*) vals) [(n' + 1.0),(n' + 2.0)..]
  in (n + (fromIntegral (length valsByIndex)), vs + (sum valsByIndex))

---------- util
say' :: Show a => String -> a -> Process ()
say' label v = say (label ++ ":" ++ (show v))

receiveReady :: a -> [Match a] -> Process a
receiveReady defVal matches = fromMaybe defVal <$> receiveTimeout 0 matches

pThreadDelay :: Int -> Process ()
pThreadDelay = liftIO . threadDelay

dumpProcState :: ProcState -> SuspectInfo -> Process ()
dumpProcState s si = do
  say' "pend cnt" (length (stPendingDeliver s))
  say' "pend" (stPendingDeliver s)
  (say . show) s { stPendingDeliver = [] }
  when trace (say' "si" si)

------------- member suspect, group view protocol
checkRefuteGossip
  :: ((ProcessId, [Integer]) -> Process ())
  -> ProcState
  -> StateT SuspectInfo Process ()
checkRefuteGossip mcastRefute' state = do
  gossips <- gets siGossip
  gossips' <-
    (lift . M.traverseWithKey (\p -> checkRefuteProc mcastRefute' p (M.lookup p (stReceiveVector state))))
      gossips
  modify (\si -> si { siGossip = gossips' })

checkRefuteProc
  :: ((ProcessId, [Integer]) -> Process ())
  -> ProcessId
  -> Maybe (Integer, a)
  -> Map ProcessId Integer
  -> Process (Map ProcessId Integer)
checkRefuteProc mcastRefute' pid mReceiveInfo clksBySuspector =
  case mReceiveInfo of
    Just (clk2,_) ->
      let (refuted, keep) = M.partition (clk2 >) clksBySuspector
      in mcastRefute' (pid, M.elems refuted) >> pure keep
    Nothing ->
      pure clksBySuspector

groupViewInstall :: Map ProcessId Integer -> State (SuspectInfo, ProcState) ()
groupViewInstall detected = do
  let (failed, clkMin) = (M.keysSet detected, minimumDef (-1) detected)
  pendingDeliver <- (\pd -> foldr (removeMsgsAfter clkMin) pd failed) <$> gets (stPendingDeliver . snd)
  foldM (\_ p -> pushClocksForward p) () failed
  modify
    (\(si, s) ->
       ( si { siEnabledViewUpdate = Just (clkMin, failed) }
       , s { stPendingDeliver = pendingDeliver } ))

pushClocksForward :: ProcessId -> State (SuspectInfo, ProcState) ()
pushClocksForward p = do
  let largeInteger = fromIntegral (maxBound :: Int)
  modify
    (\(si, s) -> 
      ( si { siStabilityVector = M.adjust (const largeInteger) p (siStabilityVector si) }
      , s { stReceiveVector = M.adjust (\(clk, tme) -> (largeInteger, tme)) p (stReceiveVector s) } ))

removeMsgsAfter :: Integer -> ProcessId -> [(a, Integer, ProcessId)] -> [(a, Integer, ProcessId)]
removeMsgsAfter clkMin from =
  filter (\(_,clk,sndr) -> not (sndr == from && clk > clkMin))

announceUpdateDetected
  :: ProcessId
  -> ProcState
  -> (Maybe (Map ProcessId Integer), SuspectInfo)
  -> Process (SuspectInfo, ProcState)
announceUpdateDetected self state (mDetected, suspectInfo) =
  case mDetected of
    Just detected -> do
      -- say (show ("confirm evidence", suspects, confirmed))
      mcast self (stMemberView state) (M.toList detected) >> say' "consensus" detected
      pure (execState (groupViewInstall detected) (suspectInfo, state))
    Nothing ->
      pure (suspectInfo, state)

recvSuspect
  :: ProcessId
  -> [ProcessId]
  -> (ProcessId, (ProcessId, Integer))
  -> State SuspectInfo (Maybe (Map ProcessId Integer))
recvSuspect self memberView (from, (suspect, lastClock)) = do
  invalidProc' <- (\ev -> invalidProc memberView ev from) <$> gets siEnabledViewUpdate
  if self == suspect || invalidProc'
    then pure Nothing
    else do
      mGossips <- gets (M.lookup suspect . siGossip)
      let gossips = (M.singleton from lastClock) <> (fromMaybe M.empty mGossips)
      suspicion <- gets siSuspicion
      modify (\si -> si { siGossip = M.insert suspect gossips (siGossip si) } )
      let suspects = M.keysSet suspicion
      confirmed <- gets (\si -> (M.keysSet . M.filterWithKey (confirmedByLive self si memberView)) suspicion)
      if suspects == confirmed && not (S.null suspects)
        then do
          modify (\si -> si { siSuspicion = M.empty })
          (pure . Just) suspicion
        else
          pure Nothing

recvConfirm
  :: ProcessId
  -> [ProcessId]
  -> (ProcessId, [(ProcessId, Integer)])
  -> State SuspectInfo (Maybe (Map ProcessId Integer))
recvConfirm self memberView (from, detected) = do
  invalidProc' <- (\ev -> invalidProc memberView ev from) <$> gets siEnabledViewUpdate
  if self == from || invalidProc'
    then pure Nothing
    else do
      let detected' = M.fromList detected
      suspicion <- gets siSuspicion
      if detected' `M.isSubmapOf` suspicion
        then do
          modify (\si -> si { siSuspicion = (siSuspicion si) `M.difference` detected' })
          (pure . Just) detected'
        else
          pure Nothing

confirmedByLive :: ProcessId -> SuspectInfo -> [ProcessId] -> ProcessId -> Integer -> Bool
confirmedByLive self state memberView suspect lastClock =
  let gossips = fromMaybe M.empty (M.lookup suspect (siGossip state))
      failed = maybe S.empty (\(_,fld) -> fld) (siEnabledViewUpdate state)
      livePeers = ((memberView \\ M.keys (siSuspicion state)) \\ [self]) \\ (S.toList failed)
  in all (maybe False (== lastClock) . (flip M.lookup) gossips) livePeers

forwardUpdateRefuted
  :: ProcessId
  -> SuspectInfo
  -> (Maybe (Map ProcessId Integer, ProcessId, Integer), ProcState)
  -> Process (SuspectInfo, ProcState)
forwardUpdateRefuted self suspectInfo result =
  case result of
    (Just (suspicion, suspect, lastClock), state) -> do
       let suspectInfo' = suspectInfo { siSuspicion = suspicion }
       when trace (say' "recv refute" (suspect, lastClock))
       mcastRefute self state suspectInfo' (suspect, [lastClock]) False -- forward for partitions?
       pure (suspectInfo', state)
    (Nothing, state) ->
       pure (suspectInfo, state)
 
recvRefute
  :: SuspectInfo
  -> POSIXTime
  -> (ProcessId, (ProcessId, Integer), [(Double, Integer, ProcessId)])
  -> State ProcState (Maybe (Map ProcessId Integer, ProcessId, Integer))
recvRefute suspectInfo now (from, (suspect, lastClock), moreRecentMsgs) = do
  let mSuspectedClock = M.lookup suspect (siSuspicion suspectInfo)
      discard = maybe False (== lastClock) mSuspectedClock
  invalidProc' <- (\mv -> invalidProc mv (siEnabledViewUpdate suspectInfo) from) <$> gets stMemberView
  if discard && not invalidProc'
    then do
      let suspicion = M.delete suspect (siSuspicion suspectInfo)
          maxClk = (maximumDef (-1) . fmap (\(_,clk,_) -> clk) . filter (\(_,_,p) -> p == suspect)) moreRecentMsgs
      modify
        (\s -> 
            s { stReceiveVector = M.adjust (\(clk,_) -> (maximum [maxClk,clk],now)) suspect (stReceiveVector s)
              , stPendingDeliver = nub (moreRecentMsgs ++ (stPendingDeliver s))
            })
      (pure . Just) (suspicion, suspect, lastClock)
    else
      pure Nothing

mcastRefute :: ProcessId -> ProcState -> SuspectInfo -> (ProcessId, [Integer]) -> Bool -> Process ()
mcastRefute self state suspectInfo (suspect, refutingClks) attachMoreRecent = do
  forM_ (stMemberView state)
    (\to ->
      (forM_ refutingClks
        (\c ->
          let receivedFromSuspect = receivedFromAfter state suspectInfo suspect c
          in (send to (self, (suspect, c), if attachMoreRecent then receivedFromSuspect else [])))))

receivedFromAfter :: ProcState -> SuspectInfo -> ProcessId -> Integer -> [(Double, Integer, ProcessId)]
receivedFromAfter state suspectInfo suspect clk =
  let pending = filter (\(_,clk2,p) -> p == suspect && clk2 > clk) (stPendingDeliver state)
      delivered = filter (\(_,clk2,p) -> p == suspect && clk2 > clk) (siUnstableDelivered suspectInfo)
  in pending ++ delivered

mcast :: Serializable a => ProcessId -> [ProcessId] -> a -> Process ()
mcast self memberView v =
  forM_ memberView ((flip send) (self, v))

failureSuspector :: Rational -> POSIXTime -> Map ProcessId (Integer, POSIXTime) -> Map ProcessId Integer
failureSuspector suspectTimeoutSecs now receiveVector =
  (M.map rcvEntryClk . M.filter suspect) receiveVector
    where
      suspect (_,last) = (now - last) > (fromRational suspectTimeoutSecs)

rcvEntryClk :: (a,b) -> a
rcvEntryClk = fst

notSuspect :: SuspectInfo -> (ProcessId, a) -> Bool
notSuspect suspectInfo (from, _) = from `M.notMember` (siSuspicion suspectInfo)

notSuspect' :: SuspectInfo -> (ProcessId, a, b) -> Bool
notSuspect' suspectInfo (from, _, _) = from `M.notMember` (siSuspicion suspectInfo)




------------------- node protocol handlers
sendBroadcast :: ((Double, Integer, Integer) -> Process ()) -> StateT (RA.StdGen, ProcState) Process ()
sendBroadcast mcastMsg = do
  maxDeliverableClk <- maxDeliverableDef 0 <$> gets (stReceiveVector . snd)
  modify (\(r, s) -> (r, s { stClock = (stClock s) + 1 }))
  v <-
     state
       (\(r, s) ->
           let (v, r') = RA.random r
           in (v, (r', s)))
  clk' <- gets (stClock . snd)
  (lift . mcastMsg) (v, clk', maxDeliverableClk)

mcastMsg :: ProcessId -> [ProcessId] -> (Double, Integer, Integer) -> Process ()
mcastMsg self memberView (v, c, dc) = do
  when trace (say' "mcastMsg" (self, memberView, (v, c, dc)))
  forM_ memberView (\to -> send to (v, c, self, dc))

deliverRecordDisable
  :: SuspectInfo
  -> (Maybe (Integer, ProcessId), ProcState)
  -> ([Double], (ProcState, SuspectInfo))
deliverRecordDisable suspectInfo result =
  case result of
    (Just (senderMaxDeliverableClk, sender), state) ->
      let ((delivered, mFailed), state') = runState (deliverByClockTicks (siEnabledViewUpdate suspectInfo)) state
          suspectInfo' =
            execState
              (do
                 updateStable senderMaxDeliverableClk sender delivered
                 maybe (pure ()) updateFailed mFailed)
              suspectInfo
      in (fmap (\(v, _, _) -> v) delivered, (state', suspectInfo'))
    (Nothing, state) ->
      let ((delivered, mFailed), state') = runState (deliverByClockTicks (siEnabledViewUpdate suspectInfo)) state
          suspectInfo' =
            execState
              (do
                 maybe (pure ()) updateFailed mFailed)
              suspectInfo
      in (fmap (\(v, _, _) -> v) delivered, (state', suspectInfo'))

recvBroadcast
  :: POSIXTime
  -> Maybe (Integer, Set ProcessId)
  -> (Double, Integer, ProcessId, Integer)
  -> State ProcState (Maybe (Integer, ProcessId))
recvBroadcast now mEnabledViewUpdate (v, clk, sender, senderMaxDeliverableClk) = do
  invalidProc' <- (\mv -> invalidProc mv mEnabledViewUpdate sender) <$> gets stMemberView
  if not invalidProc'
    then do
      modify
        (\s ->
            s {
              stClock = maximum [stClock s, clk]
            , stReceiveVector = M.insert sender (clk, now) (stReceiveVector s)
            , stPendingDeliver = (v, clk, sender):(stPendingDeliver s)
            })
      (pure . Just) ( senderMaxDeliverableClk, sender )
    else   
      pure Nothing

updateStable :: Integer -> ProcessId -> [(Double, Integer, ProcessId)] -> State SuspectInfo ()
updateStable senderMinReceiveClk sender delivered = do
  stabilityVector <- gets (M.insert sender senderMinReceiveClk . siStabilityVector)
  let minStableClk = minimumDef (-1) stabilityVector
  unstableDelivered <- gets (filter (\(_, clk, _) -> clk > minStableClk) . siUnstableDelivered)
  modify
    (\si ->
        si {
          siStabilityVector = stabilityVector
        , siUnstableDelivered = unstableDelivered ++ delivered
        } )

updateFailed :: Set ProcessId -> State SuspectInfo ()
updateFailed failed =
  modify
    (\si -> 
       si {
         siStabilityVector = M.filterWithKey (\p _ -> not (p `S.member` failed)) (siStabilityVector si)
       , siEnabledViewUpdate = Nothing
       } )

deliverByClockTicks
  :: Maybe (Integer, Set ProcessId)
  -> State ProcState ([(Double, Integer, ProcessId)], Maybe (Set ProcessId))
deliverByClockTicks mEnabledViewUpdate =
  go mEnabledViewUpdate Nothing []
  where 
    go mEnabledViewUpdate mRemoved delivered = do
      (mEnabledViewUpdate', mRemoved') <-
         maybe (mEnabledViewUpdate, mRemoved) (\f -> (Nothing, Just f)) <$> attemptUpdateView mEnabledViewUpdate
      canDeliver' <- gets (canDeliver . stReceiveVector)
      next <- gets (nextDeliverClk . stPendingDeliver)
      (ready, notReady) <- gets (partition (\(_,c,_) -> canDeliver' c && c == next) . stPendingDeliver)
      case ready of
        [] -> pure (delivered, mRemoved')
        _ -> do
          let readySorted = sortOn (\(_,_,sndr) -> sndr) ready
          modify (\s -> s { stPendingDeliver = notReady })
          go mEnabledViewUpdate' mRemoved' (delivered ++ readySorted)

attemptUpdateView :: Maybe (Integer, Set ProcessId) -> State ProcState (Maybe (Set ProcessId))
attemptUpdateView mEnabledViewUpdate =
  case mEnabledViewUpdate of
    Just (minFailedClk, failed) -> do
      next <- gets (nextDeliverClk . stPendingDeliver)
      crossingMinFailed <- gets (\s -> next > minFailedClk && canDeliver (stReceiveVector s) next)
      if crossingMinFailed
        then do
          modify
            (\s ->
               s { stMemberView = stMemberView s \\ S.toList failed
                 , stReceiveVector = M.filterWithKey (\p _ -> not (p `S.member` failed)) (stReceiveVector s)
                 })
          pure (Just failed)
        else
          pure Nothing
    Nothing ->
      pure Nothing

nextDeliverClk :: [(a, Integer, b)] -> Integer
nextDeliverClk = minimumDef (-1) . fmap (\(_, clk, _) -> clk)

canDeliver :: Map ProcessId (Integer, a) -> Integer -> Bool
canDeliver rv c = c <= maxDeliverableDef (-1) rv

maxDeliverableDef :: Integer -> Map ProcessId (Integer, a) -> Integer
maxDeliverableDef dflt =  minimumDef dflt . fmap fst

notSuspectMsg :: SuspectInfo -> (a, b, ProcessId, c) -> Bool
notSuspectMsg suspectInfo (_, _, sender, _) = sender `M.notMember` (siSuspicion suspectInfo)

invalidProc :: [ProcessId] -> Maybe (Integer, Set ProcessId) -> ProcessId -> Bool
invalidProc memberView mEnabledViewUpdate p =
     maybe False (\(_, failed) -> p `S.member` failed) mEnabledViewUpdate
  || not (p `elem` memberView)
