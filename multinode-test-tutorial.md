# Quick Start

Configure an actor system, start up three nodes, assert cluster state

# User Guide

## Background

### multi-jvm plugin

...

### multi-node test

### akka cluster

### akka cluster-sharding

## Getting Started

You can begin by getting familiar with the commands you will use and the sbt scope used. Since we are only aiming to implement tests which run on a single machine, we will only use the commands prefixed with "multi-jvm:". We will not use commands prefixed with "multiNode". Multi-node tests use their own sbt scope, called "multi-jvm", so your test source code will reside under `src/multi-jvm/scala/`, along with associated configuration and migration files in `src/multi-jvm/resources`.

You will need to create the following elements, defined within <TEST NAME>MultiNodeTest.scala:

### <TEST NAME>MultiNodeTest class
    
The test logic will be implemented here. You will also connect your test with the scala test framework.

### <TEST NAME>ConfigHelper object
    
Will implement logic to bridge between any custom configuration and ...

### <TEST NAME>MultiNodeConfig object

Will register appropriate config with multi-node testkit actor system initialization, as well as
define custom configuration objects to be used by nodes. Will define handles for each node.

### <TEST NAME>MultiNodeTestMultiJvmNode<NODE NUMBER> class

Node specific instances of the test class. Usually, these classes will simply reuse the generic test logic in <TEST NAME>MultiNodeTest class. Sometimes, one might choose to define the test logic in these derived classes. The "on(nodeN)"
syntax is generally simpler and more concise, when one needs to restrict test behavior to a specific node.
    
Appending suffix name to class lets multi-node testing introspection match the test class to the node. 
    
### <TEST NAME>MultiNodeSpecMultiJvmNode<NODE NUMBER>.opts file

Will also needs this file, if additional options are need by the JVM during startup. Each of these files will consist of command line arguments to pass to the jvm on startup.

## Adjust Configuration

You may want to adjust some of the following configuration values, depending on what you want to test.

### ...



## Implement Test

filter action by node

start node 1 first; join
start each additional node sequentially; join
assert cluster stat

on each node, start producing activity
    
## Run Test

start any docker services needed such as databases, queues
    
sbt> multi-jvm:test
    
You can also use multi-jvm:testOnly command

# Related Tutorials
    
....